section .data
newline_char: db '\n'
EXIT_SYSCALL_CODE equ 60
READ_SYSCALL_CODE equ 0
WRITE_SYSCALL_CODE equ 1
STDIN equ 0
STDOUT equ 1
MAX_DIGITS equ 20
RADIX equ 10
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE_SYSCALL_CODE
    pop rsi
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE_SYSCALL_CODE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov dil, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, MAX_DIGITS + 1
    mov rax, rdi
    mov rcx, MAX_DIGITS
    mov r9, RADIX
    mov byte[rsp + MAX_DIGITS], 0

    .loop:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rcx
        mov [rsp + rcx], dl
        test rax, rax
        jne .loop
    .print:
        mov rdi, rsp
        add rdi, rcx
        call print_string
        add rsp, MAX_DIGITS + 1
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    xor rdx, rdx
    .loop:
        mov r9b, byte[rdi + rdx]
        cmp r9b, byte[rsi + rdx]
        jne .bad
        inc rdx
        test r9b, r9b
        jne .loop
        ret

        .bad:
            xor rax, rax
            ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, READ_SYSCALL_CODE
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    mov al, [rsp]
    jnz .exit
    xor al, al
    .exit:
        inc rsp
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi ; buf*
    mov r13, rsi ; max length
    xor r14, r14 ; length

    .skip_whitespace:
        call read_char
        cmp al, ' '
        je .skip_whitespace
        cmp al, `\t`
        je .skip_whitespace
        cmp al, `\n`
        je .skip_whitespace
    
    .loop:
        cmp r13, r14
        je .overflow
        mov [r12 + r14], al
        inc r14
        ;checks for whitespace and end
        test al, al
        je .break
        cmp al, ' '
        je .break
        cmp al, `\t`
        je .break
        cmp al, `\n`
        je .break

        call read_char
        jmp .loop
    .break:
        dec r14
        mov rax, r12
        mov rdx, r14
        jmp .exit
    .overflow:
        xor rax, rax
        xor rdx, rdx
    .exit:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; result
    xor r9, r9 ; count
    mov r10, RADIX
    xor r11, r11 ; buf

    .next:
        mov r11b, [rdi + r9]
        cmp r11b, '0'
        jl .done
        cmp r11b, '9'
        jg .done
    
        inc r9
        mul r10
        add rax, r11
        sub rax, '0'

        jmp .next

    .done:
        mov rdx, r9
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    push r13
    xor r13, r13 ; is there as sign
    xor r12, r12 ; sign
    xor rax, rax ; result
    mov al, [rdi]
    cmp al, '-'
    je .neg
    cmp al, '+'
    je .pos
    jmp .read
    .neg:
        inc r12
    .pos:
        inc rdi
        inc r13
        jmp .read
    .read:
        call parse_uint
        test rdx, rdx
        je .exit
        add rdx, r13
        test r12, r12
        je .exit
        neg rax
    .exit:
        pop r13
        pop r12
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rdx, rcx
        je .fail
        mov al, [rdi + rcx]
        mov [rsi + rcx], al
        inc rcx
        test al, al
        jne .loop
        mov rax, rcx
        ret
    .fail:
        xor rax, rax
        ret
